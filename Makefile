# Makefile for mkinitcpio-ephemeral
# Copyright (C) 2018 [RnD]²
# 
# This file is part of mkinitcpio-ephemeral.
# 
# mkinitcpio-ephemeral is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# mkinitcpio-ephemeral is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with mkinitcpio-ephemeral.  If not, see <http://www.gnu.org/licenses/>.
# 
# Author: Evangelos Rigas <erigas@rnd2.org>

.PHONY: install

all: install

install:
	# install binary
	install -D -m0755 bin/prepare-ephemeral-root $(DESTDIR)/usr/lib/mkinitcpio-ephemeral/prepare-ephemeral-root
	
	# install script and unit file
	install -D -m0644 install/sd-ephemeral $(DESTDIR)/usr/lib/initcpio/install/sd-ephemeral
	install -D -m0644 systemd/mkinitcpio-ephemeral.service $(DESTDIR)/usr/lib/systemd/system/mkinitcpio-ephemeral.service
	
	# License and README
	install -D -m0644 README.md $(DESTDIR)/usr/share/doc/mkinitcpio-ephemeral/README.md
	install -D -m0644 COPYING $(DESTDIR)/usr/share/doc/mkinitcpio-ephemeral/COPYING

