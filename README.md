mkinitcpio-ephemeral
===================

Adds a initrd systemd hook that allows to boot to an ephemeral btrfs snapshot.

# Usage

## Initcpio
Edit the HOOKS variable and add the `sd-ephemeral` hook after the `systemd` in **/etc/mkinitcpio.conf**

For example on a minimal setup:
```bash
HOOKS="autodetect systemd sd-ephemeral"
```

NOTE: This will add the hook to all the generated images.

Alternatively, the `sd-ephemeral` hook can be added using the default options
in mkinitcpio presets.

Here is an example of a preset that generates two images, a default and an ephemeral one.



```bash
# /etc/mkinitcpio.d/linux-ephemeral.preset
# mkinitcpio preset file for the 'linux' package

ALL_config="/etc/mkinitcpio.conf"
ALL_kver="/boot/vmlinuz-linux"

PRESETS=('default' 'ephemeral')

#default_config="/etc/mkinitcpio.conf"
default_image="/boot/initramfs-linux.img"
#default_options=""

#ephemeral_config="/etc/mkinitcpio.conf"
ephemeral_image="/boot/initramfs-linux-ephemeral.img"
ephemeral_options="-A sd-ephemeral"
```

To generate the image `sudo mkinitcpio -p linux-ephemeral`
```bash
sudo mkinitcpio -p linux-ephemeral                                                                 
==> Building image from preset: /etc/mkinitcpio.d/linux-ephemeral.preset: 'default'
  -> -k /boot/vmlinuz-linux -c /etc/mkinitcpio.conf -g /boot/initramfs-linux.img
==> Starting build: 4.15.7-1
  -> Running build hook: [autodetect]
  -> Running build hook: [systemd]
  -> Running build hook: [modconf]
==> Generating module dependencies
==> Creating lz4-compressed initcpio image: /boot/initramfs-linux.img
==> Image generation successful
==> Building image from preset: /etc/mkinitcpio.d/linux-ephemeral.preset: 'ephemeral'
  -> -k /boot/vmlinuz-linux -c /etc/mkinitcpio.conf -g /boot/initramfs-linux-ephemeral.img -A sd-ephemeral
==> Starting build: 4.15.7-1
  -> Running build hook: [autodetect]
  -> Running build hook: [systemd]
  -> Running build hook: [modconf]
  -> Running build hook: [keyboard]
  -> Running build hook: [sd-ephemeral]
==> Generating module dependencies
==> Creating lz4-compressed initcpio image: /boot/initramfs-linux-ephemeral.img
==> Image generation successful

```

Now the initramfs image is ready.


## Bootloader

To boot into the ephemeral image the following kernel parameters must be set.
### Flags
- root (e.g. `root=/dev/sda2`, `root=LABEL=ROOT`)
  - This is the standard kernel parameter used to select the root device.
  It can be the device or any valid `UUID`, `PARTUUID`, `LABEL`.
- rootflags (e.g. `rootflags=rw,noatime,nossd,space_cache,subvol=@snapshots/@ephemeral`)
  - This flags are used when the root filesystem is mounted.
  In this case this is the ephemeral subvolume.
  - **Notice** the `subvol` flag at the end. It needs to be set to `<snapshot_path>/@ephemeral`
  where `snapshot_path` is the subvolume that will contain the ephemeral snapshot.  
  - See `snap_path`.
- root_subvol (e.g. `root_subvol=@`, `root_subvol=@snapshots/root/2018-01-01`)
  - This must contain the subvolume path of the root tree that will be used.  
  - Usually, on btrfs systems with split `/` and `/home` the root is installed under
  `/@` and home under `/@home`.
  In that case, to use `@` as the base of the ephemeral snapshot `root_subvol` must be set to `@`.  
  - Alternatively, a snapshot of the root can be used.
  Assuming that the snapshot is located under `/@snapshots/root/2018-01-01`, then
  `root_subvol` must be set to `root_subvol=@snapshots/root/2018-01-01`.
  - Lastly, this subvolume can be any valid os tree that contains the same kernel as the one used at boot.
  - **Important**: The path must be absolute and start from the root of the device.
- snap_path (e.g. `snap_path=@snapshots`, `snap_path=images/`)
  - Similar to `root_subvol`, this will be the subvolume path under which the ephemeral snapshot will be created.
  - **Important**: The path must be absolute and start from the root of the device.

#### Notes
Both `root_subvol` and `snap_path` must be valid subvolumes of the same root device (`root`).
The `rootflags` must contain the `subvol=` and be set to the path of the ephemeral snapshot.
As the snapshot will always be naned `@ephemeral` is will be the `snap_path`/`@ephemeral`.

### Examples

#### Example 1

root using `PARTUUID`, the root subvolume is `@` and the snapshot path at `snaps`

Disk Layout:
~~~
/dev/disk/by-partuuid/65e51edbd-0703-43f5-802a-de30001b7c3c # root
├── @ # root_subvol
├── @home
└── snaps # snap_path
    └── @ephemeral # rootflags subvol=snaps/@ephemeral (it will be created by this hook)
~~~

kernel options: `root=PARTUUID=65e51edbd-0703-43f5-802a-de30001b7c3c rootflags=rw,noatime,nossd,space_cache,subvol=snaps/@ephemeral root_subvol=@ snap_path=snaps quiet`

#### Example 2
root using `PARTUUID`, the root subvolume is `@` and the snapshot path at `snaps`

Disk Layout:
~~~
/dev/sda2 # root
├── @ # default root
├── images
│   ├── arch
│   │   ├── development # root_subvol
│   │   ├── gaming
│   │   └── snaps # snap_path
│   │       └── @ephemeral # rootflags subvol=images/arch/snaps/@ephemeral (it will be created by this hook)
│   ├── debian
│   │   ├── sid
│   │   └── stretch
│   │   └── snaps
│   └── fedora
│       ├── 26
│       └── 27
└── snaps
    └── root
        └── 2018-01-10
~~~
kernel options: `root=/dev/sda2 rootflags=rw,noatime,nossd,space_cache,subvol=images/arch/snaps/@ephemeral root_subvol=images/arch/development snap_path=images/arch/snaps quiet`

#### Example 3
root using `PARTUUID`, the root subvolume is `@` and the snapshot path at `snaps`

Disk Layout:
~~~
/dev/sda2 # root
├── @ # default root
└── snaps # snap_path
    └── root
    │   └── 2018-01-10 # root_subvol
    └── @ephemeral # rootflags subvol=snaps/@ephemeral (it will be created by this hook)
~~~
kernel options: `root=/dev/sda2 rootflags=rw,noatime,nossd,space_cache,subvol=snaps/@ephemeral root_subvol=snaps/root/2018-01-10 snap_path=snaps quiet`

# License

Copyright (C) 2018 [RnD]²

This file is part of mkinitcpio-ephemeral.

mkinitcpio-ephemeral is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

mkinitcpio-ephemeral is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with mkinitcpio-ephemeral.  If not, see <http://www.gnu.org/licenses/>.
